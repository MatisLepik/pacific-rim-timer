### Pacific Rim timer ###

Pretty short and simple little timer I made for fun. I haven't seen Pacific Rim but it turns out they have a really cool [war clock](http://vignette2.wikia.nocookie.net/pacificrim/images/6/68/Pr-warclock.jpg/revision/20140705055509). So I decided to try and recreate it using JavaScript. Some browsers might still not support blending modes, but it looks alright even without it.

**You can see a [live version](http://matislepik.eu/prtimer/) on my website.**

### Contact ###

- Matis Lepik

- matis.lepik@gmail.com

- www.matislepik.eu